> 🏷️: `C# 10`￤`.NET 6`￤`dapper`￤`sqlite`

# XPTracker's Tracker


![C# Player’s Guide](./doc/csharp_players_guide_portrait.png)


Hace unos días adquirí el libro [*The C# Player's Guide (5th Edition)*](https://csharpplayersguide.com/) y hasta el día de hoy he quedado encantado con todo el aporte que está haciendo en mi vida como programador junior.


![XPTracker Print](./doc/xp_tracker.png)


Este proyecto fue creado con la intención de integrar algunos conocimientos recién adquiridos. Lo que se intenta replicar es el funcionamiento del XPTracker existente en el libro el cual se usa (como su nombre menciona) para hacer *seguimiento* a los desafíos que el lector/programador va resolviendo, los cuales aportan cierta cantidad de puntos de experiencia que se van acumulando a lo largo de la hermosa historia que en paralelo va contando el libro.


![XPTracker Form](./doc/xptracker_form.png)

---
_Si estás interesado en adquirir el libro, puedes hacerlo desde los siguientes links: [Amazon](https://www.amazon.com/dp/0985580151) | [Web del Autor](https://rbwhitaker.gumroad.com/l/zGNbnc)_
