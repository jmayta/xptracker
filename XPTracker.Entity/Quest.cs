﻿namespace XPTracker.Entity
{
    public class Quest
    {
        public int Id { get; set; }
        public int Page { get; set; }
        public string Name { get; set; }
        public int Experience { get; set; }
        public int IdStage { get; set; }
        public bool IsDone { get; set; }
        public Stage ObjectStage { get; set; }
    }
}
