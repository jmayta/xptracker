﻿using System.ComponentModel.DataAnnotations.Schema;

namespace XPTracker.Entity
{
    public class Edition
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public List<Stage>? Stages { get; set; }
    }
}