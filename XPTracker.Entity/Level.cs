﻿namespace XPTracker.Entity
{
    public class Level
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MinExp { get; set; }
    }
}
