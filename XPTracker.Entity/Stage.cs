﻿namespace XPTracker.Entity
{
    public class Stage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IdEdition { get; set; }
        public Edition ObjectEdition { get; set; }
        public List<Quest> Quests { get; set; }
    }
}
