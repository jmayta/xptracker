﻿using Dapper;
using Microsoft.Data.Sqlite;
using XPTracker.Entity;

namespace XPTracker.Engine
{
    public class StageEngine
    {
        public List<Stage> GetStages(Edition edition)
        {
            List<Stage> stages = new List<Stage>();
            string sql = "SELECT * FROM Stages WHERE id_edition = @idEdition";
            using (SqliteConnection connection = new SqliteConnection(
                DatabaseHelper.ConnectionString)
            )
            {
                stages = connection.Query<Stage>(
                    sql, new { idEdition = edition.Id }
                    ).ToList();
            }

            return stages;
        }
    }
}
