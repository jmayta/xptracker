﻿using System.Configuration;

namespace XPTracker.Engine
{
    public class DatabaseHelper
    {
        private string _connectionString;

        public static string ConnectionString {  
            get
            {
                return ConfigurationManager.ConnectionStrings["XPTracker"].ConnectionString;
            }
        }
    }
}
