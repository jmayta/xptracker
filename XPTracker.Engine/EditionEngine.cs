﻿using Dapper;
using Microsoft.Data.Sqlite;
using XPTracker.Entity;

namespace XPTracker.Engine
{
    public class EditionEngine
    {
        public List<Edition> GetEditions()
        {
            List<Edition> editions = new List<Edition>();
            using (SqliteConnection connection = new SqliteConnection(DatabaseHelper.ConnectionString))
            {
                var sql = "SELECT id, name FROM Editions";
                editions = connection.Query<Edition>(sql).ToList();
                return editions;
            }
        }
    }
}