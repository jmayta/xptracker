﻿using Dapper;
using Microsoft.Data.Sqlite;
using XPTracker.Entity;

namespace XPTracker.Engine
{
    public class LevelEngine
    {
        public Level GetCurrentLevel(int exp)
        {
            Level currentLevel = new Level();

            string sql = @"
                SELECT id, name, min_experience FROM Levels
                WHERE min_experience <= @exp
                ORDER BY min_experience DESC
                LIMIT 1;
            ";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@exp", exp);

            using (SqliteConnection connection = new SqliteConnection(
                DatabaseHelper.ConnectionString))
            {
                currentLevel = connection.QuerySingleOrDefault<Level>(sql, parameters);
            }
            return currentLevel;
        }

        public Level GetNextLevel(int exp)
        {
            Level nextLevel = new Level();

            string sql = @"
                SELECT id, name, min_experience AS MinExp FROM Levels
                WHERE min_experience > @exp
                ORDER BY min_experience ASC
                LIMIT 1;
            ";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@exp", exp);

            using (SqliteConnection connection = new SqliteConnection(
                DatabaseHelper.ConnectionString))
            {
                nextLevel = connection.QuerySingleOrDefault<Level>(sql, parameters);
                return nextLevel;
            }
        }

        public int GetMaxExperience()
        {
            int maxExp = 0;

            string sql = @"
                SELECT MAX(min_experience) AS MaxExperience FROM Levels;
            ";

            using (SqliteConnection connection = new SqliteConnection(
                DatabaseHelper.ConnectionString))
            {
                maxExp = connection.ExecuteScalar<int>(sql);
            }

            return maxExp;
        }
    }
}
