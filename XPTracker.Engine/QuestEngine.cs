﻿using Dapper;
using Microsoft.Data.Sqlite;
using XPTracker.Entity;

namespace XPTracker.Engine
{
    public class QuestEngine
    {
        public List<Quest> GetQuests(Stage stage)
        {
            List<Quest> quests = new List<Quest>();
            string sql = @"
                SELECT id, is_done AS IsDone, page, name, experience, id_stage AS IdStage
                FROM Quests WHERE id_stage = @idStage;
            ";
            using (SqliteConnection connection = new SqliteConnection(
                DatabaseHelper.ConnectionString))
            {
                quests = connection.Query<Quest>(
                    sql, new { idStage = stage.Id }
                ).ToList();
            }
            return quests;
        }

        public int ToggleQuestCompletion(Quest quest)
        {
            int affectedRows = 0;

            string sql = "UPDATE Quests SET is_done = @isDone WHERE id = @idQuest";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@isDone", quest.IsDone);
            parameters.Add("@idQuest", quest.Id);

            using (SqliteConnection connection = new SqliteConnection(
                DatabaseHelper.ConnectionString))
            {
                affectedRows = connection.Execute(sql, parameters);
            }

            return affectedRows;
        }

        public int GetExperience(Edition edition)
        {
            int totalExp = 0;
            string sql = @"
                SELECT SUM(q.experience) as TotalExp
                FROM Editions e
                INNER JOIN Stages s ON e.id = s.id_edition
                INNER JOIN Quests q ON s.id = q.id_stage
                WHERE e.id = @EditionId AND q.is_done = 1; 
            ";

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@EditionId", edition.Id);

            using (SqliteConnection connection = new SqliteConnection(DatabaseHelper.ConnectionString))
            {
                totalExp = connection.ExecuteScalar<int>(sql, parameters);
            }

            return totalExp;
        }
    }
}
