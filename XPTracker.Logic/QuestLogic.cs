﻿using System.ComponentModel;
using XPTracker.Engine;
using XPTracker.Entity;

namespace XPTracker.Logic
{
    public class QuestLogic
    {
        private readonly QuestEngine questEngine = new QuestEngine();

        public BindingList<Quest> GetQuests(Stage stage)
        {
            return new BindingList<Quest>(questEngine.GetQuests(stage));
        }

        public int ToggleQuestCompletion(Quest quest)
        {
            return questEngine.ToggleQuestCompletion(quest);
        }

        public int GetExperience(Edition edition)
        {
            return questEngine.GetExperience(edition);
        }
    }
}
