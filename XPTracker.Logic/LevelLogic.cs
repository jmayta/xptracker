﻿using XPTracker.Engine;
using XPTracker.Entity;

namespace XPTracker.Logic
{
    public class LevelLogic
    {
        LevelEngine levelEngine = new LevelEngine();

        public Level GetCurrentLevel(int exp)
        {
            return levelEngine.GetCurrentLevel(exp);
        }

        public Level GetNextLevel(int exp)
        {
            return levelEngine.GetNextLevel(exp);
        }

        public int GetMaxExperience()
        {
            return levelEngine.GetMaxExperience();
        }

    }
}
