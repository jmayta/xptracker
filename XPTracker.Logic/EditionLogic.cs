﻿using System.ComponentModel;
using XPTracker.Engine;
using XPTracker.Entity;

namespace DataLayer
{
    public class EditionLogic
    {
        private readonly EditionEngine editionEngine = new EditionEngine();

        public BindingList<Edition> GetEditions()
        {
            return new BindingList<Edition>(editionEngine.GetEditions());
        }

    }
}