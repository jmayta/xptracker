﻿using System.ComponentModel;
using XPTracker.Engine;
using XPTracker.Entity;

namespace XPTracker.Logic
{
    public class StageLogic
    {
        private readonly StageEngine stageEngine = new StageEngine();

        public BindingList<Stage> GetStages(Edition edition)
        {
            return new BindingList<Stage>(stageEngine.GetStages(edition));
        }
    }
}
