using DataLayer;
using XPTracker.Entity;
using XPTracker.Logic;

namespace UI
{
    public partial class frmTracker : Form
    {
        private readonly EditionLogic editionLogic = new EditionLogic();
        private readonly StageLogic stageLogic = new StageLogic();
        private readonly QuestLogic questLogic = new QuestLogic();
        private readonly LevelLogic levelLogic = new LevelLogic();

        private int _actualExp, _nextLevelExp;
        private Level _nextLevel;

        public frmTracker()
        {
            InitializeComponent();
        }

        private void UpdateProgressBar()
        {
            _actualExp = questLogic.GetExperience(
                (Edition)tscmbEdition.ComboBox.SelectedItem
                );
            _nextLevel = levelLogic.GetNextLevel(_actualExp);
            _nextLevelExp = _nextLevel.MinExp;
            pbrExperience.Maximum = _nextLevelExp;
            pbrExperience.Value = _actualExp;

            tsslblEdition.Text = ((Edition)tscmbEdition.ComboBox.SelectedItem).Name;
            tsslblExperience.Text = $"{_actualExp} / {_nextLevelExp}";
            tsslblCurrentLevel.Text = levelLogic.GetCurrentLevel(_actualExp).Name;
        }

        private void frmTracker_Load(object sender, EventArgs e)
        {
            tscmbEdition.ComboBox.DataSource = editionLogic.GetEditions();
            tscmbEdition.ComboBox.DisplayMember = "Name";
            tscmbEdition.ComboBox.ValueMember = "Id";
        }

        private void tscmbEditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            tscmbStage.ComboBox.DataSource = stageLogic.GetStages(
                (Edition)tscmbEdition.ComboBox.SelectedItem);
            tscmbStage.ComboBox.DisplayMember = "Name";
            tscmbStage.ComboBox.ValueMember = "Id";

            UpdateProgressBar();
        }

        private void tscmbStage_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvQuests.AutoGenerateColumns = false;

            dgvQuests.DataSource = questLogic.GetQuests(
                (Stage)tscmbStage.ComboBox.SelectedItem);
            dgvtxtId.DataPropertyName = "Id";
            dgvchkIsDone.DataPropertyName = "IsDone";
            dgvtxtPage.DataPropertyName = "Page";
            dgvtxtName.DataPropertyName = "Name";
            dgvtxtExperience.DataPropertyName = "Experience";
        }

        private void dgvQuests_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgvQuests.IsCurrentCellDirty)
            {
                dgvQuests.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }

        }

        private void dgvQuests_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (
                dgvQuests.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn &&
                e.RowIndex >= 0
                )
            {
                var currentQuestRow = dgvQuests.Rows[e.RowIndex].DataBoundItem as Quest;

                questLogic.ToggleQuestCompletion(currentQuestRow);

                UpdateProgressBar();
            }
        }
    }
}