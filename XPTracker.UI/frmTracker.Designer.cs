﻿namespace UI
{
    partial class frmTracker
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            dgvQuests = new DataGridView();
            statusStrip1 = new StatusStrip();
            tsslblEdition = new ToolStripStatusLabel();
            toolStripStatusLabel2 = new ToolStripStatusLabel();
            tsslblCurrentLevel = new ToolStripStatusLabel();
            toolStripStatusLabel1 = new ToolStripStatusLabel();
            tsslblExperience = new ToolStripStatusLabel();
            pbrExperience = new ProgressBar();
            menuStrip1 = new MenuStrip();
            tscmbEdition = new ToolStripComboBox();
            tscmbStage = new ToolStripComboBox();
            dgvtxtId = new DataGridViewTextBoxColumn();
            dgvchkIsDone = new DataGridViewCheckBoxColumn();
            dgvtxtPage = new DataGridViewTextBoxColumn();
            dgvtxtName = new DataGridViewTextBoxColumn();
            dgvtxtExperience = new DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)dgvQuests).BeginInit();
            statusStrip1.SuspendLayout();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // dgvQuests
            // 
            dgvQuests.AllowUserToAddRows = false;
            dgvQuests.AllowUserToDeleteRows = false;
            dgvQuests.AllowUserToResizeColumns = false;
            dgvQuests.AllowUserToResizeRows = false;
            dgvQuests.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = SystemColors.Control;
            dataGridViewCellStyle1.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            dgvQuests.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dgvQuests.Columns.AddRange(new DataGridViewColumn[] { dgvtxtId, dgvchkIsDone, dgvtxtPage, dgvtxtName, dgvtxtExperience });
            dgvQuests.Dock = DockStyle.Fill;
            dgvQuests.Location = new Point(0, 27);
            dgvQuests.MultiSelect = false;
            dgvQuests.Name = "dgvQuests";
            dgvQuests.RowTemplate.Height = 25;
            dgvQuests.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvQuests.Size = new Size(800, 378);
            dgvQuests.TabIndex = 0;
            dgvQuests.CellValueChanged += dgvQuests_CellValueChanged;
            dgvQuests.CurrentCellDirtyStateChanged += dgvQuests_CurrentCellDirtyStateChanged;
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { tsslblEdition, toolStripStatusLabel2, tsslblCurrentLevel, toolStripStatusLabel1, tsslblExperience });
            statusStrip1.Location = new Point(0, 428);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(800, 22);
            statusStrip1.TabIndex = 1;
            statusStrip1.Text = "statusStrip1";
            // 
            // tsslblEdition
            // 
            tsslblEdition.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            tsslblEdition.Name = "tsslblEdition";
            tsslblEdition.Size = new Size(70, 17);
            tsslblEdition.Text = "[edition]";
            // 
            // toolStripStatusLabel2
            // 
            toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            toolStripStatusLabel2.Size = new Size(19, 17);
            toolStripStatusLabel2.Text = "￤";
            // 
            // tsslblCurrentLevel
            // 
            tsslblCurrentLevel.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            tsslblCurrentLevel.Name = "tsslblCurrentLevel";
            tsslblCurrentLevel.Size = new Size(56, 17);
            tsslblCurrentLevel.Text = "[level]";
            // 
            // toolStripStatusLabel1
            // 
            toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            toolStripStatusLabel1.Size = new Size(19, 17);
            toolStripStatusLabel1.Text = "￤";
            // 
            // tsslblExperience
            // 
            tsslblExperience.Font = new Font("Consolas", 9F, FontStyle.Regular, GraphicsUnit.Point);
            tsslblExperience.Name = "tsslblExperience";
            tsslblExperience.Size = new Size(42, 17);
            tsslblExperience.Text = "[exp]";
            // 
            // pbrExperience
            // 
            pbrExperience.Dock = DockStyle.Bottom;
            pbrExperience.Location = new Point(0, 405);
            pbrExperience.Maximum = 200;
            pbrExperience.Name = "pbrExperience";
            pbrExperience.Size = new Size(800, 23);
            pbrExperience.Step = 1;
            pbrExperience.TabIndex = 2;
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { tscmbEdition, tscmbStage });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(800, 27);
            menuStrip1.TabIndex = 3;
            menuStrip1.Text = "menuStrip1";
            // 
            // tscmbEdition
            // 
            tscmbEdition.DropDownStyle = ComboBoxStyle.DropDownList;
            tscmbEdition.Name = "tscmbEdition";
            tscmbEdition.Size = new Size(121, 23);
            tscmbEdition.SelectedIndexChanged += tscmbEditions_SelectedIndexChanged;
            // 
            // tscmbStage
            // 
            tscmbStage.DropDownStyle = ComboBoxStyle.DropDownList;
            tscmbStage.Name = "tscmbStage";
            tscmbStage.Size = new Size(121, 23);
            tscmbStage.SelectedIndexChanged += tscmbStage_SelectedIndexChanged;
            // 
            // dgvtxtId
            // 
            dgvtxtId.HeaderText = "Id";
            dgvtxtId.Name = "dgvtxtId";
            dgvtxtId.ReadOnly = true;
            dgvtxtId.Visible = false;
            // 
            // dgvchkIsDone
            // 
            dgvchkIsDone.FillWeight = 10F;
            dgvchkIsDone.HeaderText = "Done";
            dgvchkIsDone.Name = "dgvchkIsDone";
            // 
            // dgvtxtPage
            // 
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvtxtPage.DefaultCellStyle = dataGridViewCellStyle2;
            dgvtxtPage.FillWeight = 10F;
            dgvtxtPage.HeaderText = "Page";
            dgvtxtPage.Name = "dgvtxtPage";
            dgvtxtPage.ReadOnly = true;
            // 
            // dgvtxtName
            // 
            dgvtxtName.HeaderText = "Name";
            dgvtxtName.Name = "dgvtxtName";
            dgvtxtName.ReadOnly = true;
            // 
            // dgvtxtExperience
            // 
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvtxtExperience.DefaultCellStyle = dataGridViewCellStyle3;
            dgvtxtExperience.FillWeight = 10F;
            dgvtxtExperience.HeaderText = "XP";
            dgvtxtExperience.Name = "dgvtxtExperience";
            dgvtxtExperience.ReadOnly = true;
            // 
            // frmTracker
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(dgvQuests);
            Controls.Add(pbrExperience);
            Controls.Add(statusStrip1);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "frmTracker";
            Text = "The C# Player's Guide￤XPTracker";
            Load += frmTracker_Load;
            ((System.ComponentModel.ISupportInitialize)dgvQuests).EndInit();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private TabPage tabPage5;
        private DataGridView dgvQuests;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel tsslblCurrentLevel;
        private ProgressBar pbrExperience;
        private DataGridView dgvPart2;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridView dgvPart3;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridView dgvPart4;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridView dgvPart5;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private MenuStrip menuStrip1;
        private ToolStripComboBox tscmbEdition;
        private ToolStripComboBox tscmbStage;
        private ToolStripStatusLabel tsslblEdition;
        private ToolStripStatusLabel tsslblExperience;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private DataGridViewTextBoxColumn dgvtxtId;
        private DataGridViewCheckBoxColumn dgvchkIsDone;
        private DataGridViewTextBoxColumn dgvtxtPage;
        private DataGridViewTextBoxColumn dgvtxtName;
        private DataGridViewTextBoxColumn dgvtxtExperience;
    }
}